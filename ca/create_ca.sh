#!/bin/bash 
P="/opt/ca"
cp /etc/pki/tls/openssl.cnf .
sed -i "s^/etc/pki/CA^${P}^g" openssl.cnf
sed -i "s^# copy_extensions = copy^copy_extensions = copy^g" openssl.cnf

cat >>openssl.cnf <<EOF
[ v3_server ]
basicConstraints = CA:FALSE
nsCertType			= server
nsComment			= "OpenSSL Generated Certificate"
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer:always
keyUsage = critical, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth
EOF

touch index.txt 
echo 1000 > serial 
mkdir certs newcerts
openssl req -newkey rsa:4096 -new -days 3650 -nodes -sha256 -keyout myca.key -extensions v3_ca -subj "/C=US/ST=North Carolina/L=Raleigh/O=Red Hat/OU=Summit/CN=IAMSummitCA" -out myca.csr -extensions v3_ca -config openssl.cnf 
openssl rsa -in myca.key -out myca-rsa.key 
chmod 600 myca*.key 
openssl ca -config openssl.cnf -extensions v3_ca -days 3650 -notext -md sha256 -in myca.csr -out myca.crt -keyfile myca.key -cert myca.crt -batch -selfsign
