#!/bin/bash
if [ -z "$1" ]; then
 echo "You must specify your WILDCARD DOMAIN."
 exit 1
fi

sed -i "s/WILDCARD/${1}/g" dc.yaml
