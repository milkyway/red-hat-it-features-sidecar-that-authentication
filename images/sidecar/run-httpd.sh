#!/usr/bin/bash
cp /etc/config/metadata.xml /tmp/metadata.xml
sed -i "s^#ServerName www.example.com:8443^ServerName ${SERVERNAME}^g" /etc/httpd/conf.d/ssl.conf
exec /usr/sbin/apachectl -DFOREGROUND
